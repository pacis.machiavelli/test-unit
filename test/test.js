const { factorial } = require('../src/util.js');
const { odd_or_even } = require('../src/util.js');
const { div_check } = require('../src/util.js');

const { expect, assert } = require('chai');

describe('test_fun_factorials', () => { 

	it('test_fun_factorial_5!_is_120', () => {
		const product = factorial(5);
		expect(product).to.equal(120);
	});

	it('test_fun_factorial_1!_is_1', () => {
		const product = factorial(1);
		assert.equal(product, 1);
	});

/*activity!*/

	it('test_fun_factorial_0!', () =>{
		const product = factorial(0);
		expect(product).to.equal(1);
	});

	it('test_fun_factorial_4!', () =>{
		const product = factorial(4);
		expect(product).to.equal(24);
	});

	it('test_fun_factorial_10!', () =>{
		const product = factorial(10);
		assert.equal(product, 3628800);
	})

});

/*ACTIVITY!!*/

describe('test_fun_div_check', () => {

	it('105_should_be_divisible_by_5', () => {
		const product = div_check(105);
		expect(product).to.equal(1);
	});

	it('14_should_be_divisible_by_7', () =>{
		const product = div_check(14)
		assert.equal(product, 1);
	});

	it('0_should_be_divisible_by_5_or_7', () => {
		const product = div_check(0);
		assert.equal(product, 1);
	});

	it('22_should_not_be_divisible_by_5_or_7', () => {
		const product = div_check(22);
		assert.equal(product, 0);
	});

});

/*end of activity*/


describe('test_fun_odd_or_even', () => {

	it('test_2_is_even', () => {
		const product = odd_or_even(2);
		expect(product).to.equal('even')
	});

	it('test_5_is_odd', () => {
		const product = odd_or_even(5);
		assert.equal(product, "odd") 
	})
})


