function factorial(n){
	if(typeof n !== 'number') return undefined;
	if(n < 0 ) return undefined;
	if(n === 0) return 1;
	if(n === 1) return 1;
	return n * factorial(n - 1);
}

function odd_or_even(n) {
	if(n%2 == 0) return "even"
		else return "odd";
}

// activity

function div_check(n) {
	if(n%7 === 0 || n%5 === 0) return 1;
	else return 0
}

module.exports = {
	factorial: factorial,
	odd_or_even:odd_or_even,
	div_check: div_check

}


